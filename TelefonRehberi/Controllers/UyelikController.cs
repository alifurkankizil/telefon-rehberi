﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers
{
    public class UyelikController : Controller
    {
        ProjeUye db = new ProjeUye();
        ProjeDb RehberDb = new ProjeDb();
        // GET: Uyelik
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Kayit()
        {
            if (Session["email"] != null)
            {
                return RedirectToAction("OturumAcik");
            }
            return View();
        }
        
        [HttpPost]
        public ActionResult Kayit(Uye U)
        {
            if (ModelState.IsValid)
            {
                var sorgu = db.Uyes.Where(x => x.Email == U.Email).Count();
                if (sorgu == 0)
                {
                    db.Uyes.Add(U);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
                else if (sorgu == 1)
                {
                    ViewBag.uyari = "Bu mail adresi kullanimda";
                }
                
            }
            return View(U);
        }

        public ActionResult Giris()
        {
            if (Session["email"] != null)
            {
                return RedirectToAction("OturumAcik");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Giris(Uye U)
        {
            var sayi = db.Uyes.Where(x => x.Email == U.Email && x.Sifre==U.Sifre).Count();

            if (sayi == 1)//Giriş başarılı
            {
                Session["email"] = U.Email;
                return RedirectToAction("Index","Home");
            }
            else if (sayi == 0)
            {
                ViewBag.uyari = "Hatali kullanici adi yada sifre girdiniz ";
            }
            return View(U);
        }
        public ActionResult KullaniciPaneli()
        {
            string kullanici = Convert.ToString(Session["email"]);
            if (Session["email"] != null)
            {
                var sorgu = RehberDb.Rehbers.Where(x => x.Ekleyen == kullanici && x.Silinme != "1");
                return View(sorgu.ToList());
            }
            else
            {
                
                return RedirectToAction("Hata","Uyelik");
            }
        }

        public ActionResult Silme(int id)
        {

            var sorgu = RehberDb.Rehbers.Find(id);
            if (sorgu.Ekleyen == Convert.ToString(Session["email"]))
            {
                sorgu.Silinme = "1";
                RehberDb.SaveChanges();
                return RedirectToAction("KullaniciPaneli", "Uyelik");
            }
            else
            {
                return RedirectToAction("Hata", "Uyelik");
            }
        }

        public ActionResult Hata()
        {
            return View();
        }

        public ActionResult OturumAcik()
        {
            return View();
        }

        public ActionResult Cikis()
        {
            Session.Remove("email");
            return RedirectToAction("Index", "Home");
        }
        
    
    }
}
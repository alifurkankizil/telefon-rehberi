﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers
{
    public class HomeController : Controller
    {
        ProjeDb db = new ProjeDb();
        ProjeSehir dbSehir = new ProjeSehir();


        /************************************************************************************
                                Anasayfa-Rehberin Listelenmesi
        ************************************************************************************/
        public ActionResult Index(int sayfa = 1)
        {
            var sorgu = db.Rehbers.Where(x => x.Silinme != "1");

            return View(sorgu.OrderBy(x => x.Ad).ToPagedList(sayfa,6));
        }



        /************************************************************************************
                                Ekleme Viewi ve Controller
        ************************************************************************************/
        public ActionResult Ekle()
        {
            if (Session["email"] == null)
            {
                return RedirectToAction("Hata", "Uyelik");
            } else
            {
               
                return View();
            }

        }
        /*
        [HttpPost]
        public ActionResult Ekle(FormCollection form)
        {
            Rehber model = new Rehber();
            model.Ad = form["ad"].Trim();
            model.Soyad = form["soyad"].Trim();
            model.Telefon = form["tel"];
            db.Rehbers.Add(model);
            db.SaveChanges();
            return RedirectToAction("Index");

        }*/

        [HttpPost]
        public ActionResult Ekle(Rehber R,FormCollection form)
        {

            var kayitVarMi = db.Rehbers.Where(x => x.Telefon == R.Telefon).Count();
           

            
            if (kayitVarMi == 0)
            {
                if (ModelState.IsValid)
                {
                    db.Rehbers.Add(R);
                    var ara = db.Rehbers.Find(R.Id);
                    ara.Ekleyen = Convert.ToString(Session["email"]);
                
                    if (Request.Files.Count > 0)
                    {
                        //Uzantıyı bulma
                        string uzanti = System.IO.Path.GetExtension(Request.Files[0].FileName);
                        //Eğer jpg ve png ise devam et
                        if (uzanti == ".jpg" || uzanti == ".png")
                        {
                            //benzersiz isim oluşturma
                            string dosyaAdi = Guid.NewGuid().ToString().Replace("-", "");
                            string resimYolu = "~/Images/Rehber/" + dosyaAdi + uzanti;
                            Request.Files[0].SaveAs(Server.MapPath(resimYolu));
                            ara.Resim = dosyaAdi + uzanti;
                        }
                    }
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            
            
            else if (kayitVarMi == 1)
            {
                ViewBag.uyari= "Bu telefon adresi kullanimda";
            }
            return View(R);
            
            
        }

        


        /************************************************************************************
                                Dtay View ve Controller
        ************************************************************************************/
        
        public ActionResult Detay(int id)
        {
            return PartialView(db.Rehbers.Find(id));
        }

        /*
        public ActionResult Sil(int id)
        {
            var sorgu = db.Rehbers.Find(id);
            db.Rehbers.Remove(db.Rehbers.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }*/
            
            
            /*
        public ActionResult Sil(int id)
        {
            if (Session["email"] == null)
            {
                return RedirectToAction("Hata", "Uyelik");
            }
            else
            {
                return View(db.Rehbers.Find(id));
            }
        }*/


        /*      View dan gelen veri
         *      
        [HttpPost]
        public ActionResult Sil(Rehber R)
        {
            db.Rehbers.Remove(db.Rehbers.Find(R.Id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        */

        /************************************************************************************
                                Duzenle View ve Controller
        ************************************************************************************/
        /*public ActionResult Duzenle(int id)
        {
            if (Session["email"] == null)
            {
                return RedirectToAction("Hata", "Uyelik");
            }
            else
            {
                return View(db.Rehbers.Find(id));
            }
        }

        [HttpPost]
        public ActionResult Duzenle(Rehber R)
        {
            if (ModelState.IsValid)
            {
                var sayi = db.Rehbers.Where(x => x.Telefon == R.Telefon).Count();
                if (sayi == 0)
                {
                    var sorgu = db.Rehbers.Find(R.Id);
                    sorgu.Ad = R.Ad;
                    sorgu.Soyad = R.Soyad;
                    sorgu.Telefon = R.Telefon;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else if (sayi == 1)
                {
                    ViewBag.uyari = "Bu telefon adresi kullanimda";
                }
                
            }
            return View(R);
            
        }
        */
        /*
            public ActionResult Duzenle(int id)
        {
            return PartialView(db.Rehbers.Find(id));
        }*/

        /************************************************************************************
                                        Vazgeçme -- Anasayfa Yönlendirme
        ************************************************************************************/
        /*
        public ActionResult Vazgec()
        {
            return RedirectToAction("Index");
        }*/
        
 
  
        
     
    }
}
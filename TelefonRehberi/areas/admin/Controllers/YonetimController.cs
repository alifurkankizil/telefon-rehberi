﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelefonRehberi.Areas.Admin.Models;
using TelefonRehberi.Models;

namespace TelefonRehberi.Areas.Admin.Controllers
{
    public class YonetimController : Controller
    {
        ProjeYonetici dB = new ProjeYonetici();
        ProjeDb Rehber = new ProjeDb();
        ProjeUye Uye = new ProjeUye();

        public ActionResult Giris()
        {
            if(Request.Cookies["Yonetici"] == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
            
        }
        

        [HttpPost]
        public ActionResult Giris(Yonetici Y)
        {
            var sorgu = dB.Yoneticis.Where(x => x.Email == Y.Email && x.Sifre == Y.Sifre).Count();

            if (sorgu == 1)
            {
                HttpCookie Cookie = new HttpCookie("Yonetici");
                Cookie["Yetki"] = "1";
                Cookie.Expires = DateTime.Now.AddMinutes(150);
                    
                Response.Cookies.Add(Cookie);

                Session["yonetici"] = Y.Email;
                return RedirectToAction("Index");
            }
            return View(Y);
        }


        /*************************************************************************************
        *************************      ADMIN GIRISLERI             ***************************                  
        *************************************************************************************/

        [AdminGiris]
        public ActionResult Index()
        {
            return View();
        }



        [AdminGiris]
        public ActionResult Silinenler()
        {
            var sorgu = Rehber.Rehbers.Where(x => x.Silinme == "1");
            return View(sorgu.ToList());
        }
        

        [AdminGiris]
        public ActionResult Cikis()
        {
            Response.Cookies["Yonetici"].Expires = DateTime.Now.AddDays(-150);
            return RedirectToAction("Giris");
        }

        [AdminGiris]
        public ActionResult KullaniciSil()
        {
            var sorgu = Uye.Uyes.ToList();
            return View(sorgu);
        }

        [AdminGiris]
        public ActionResult KullaniciSil_Id(int id)
        {
            var sorgu = Uye.Uyes.Find(id);
            Uye.Uyes.Remove(sorgu);
            Uye.SaveChanges();
            return RedirectToAction("KullaniciSil");
        }
   
        [AdminGiris]
        public ActionResult GeriYukle(int id)
        {
            var sorgu = Rehber.Rehbers.Find(id);
            sorgu.Silinme = "NULL";
            Rehber.SaveChanges();
            return RedirectToAction("Silinenler");
        }

    }
}
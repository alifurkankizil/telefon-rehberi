﻿using System.Web;
using System.Web.Mvc;


namespace TelefonRehberi.Areas.Admin
{
    public class AdminGiris : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.Cookies["Yonetici"] != null)
            {
                return true;
            }
            else
            {
                httpContext.Response.Redirect("/Admin/Yonetim/Giris");
                return false;
            }
        }
    }
}